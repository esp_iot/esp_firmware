#include <rom/ets_sys.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_log.h"
#include "driver/gpio.h"
#include "nvs.h"
#include "nvs_flash.h"
#include "string.h"

#include "cfg.h"
#include "net.h"
#include "fallback.h"
#include "ota.h"

/* Macro for the FULL COMMIT HASH of the current build */
#ifndef VERSION_BUILD_COMMIT
#define VERSION_BUILD_COMMIT "a5a5a5a5a5a5a5a5a5a5a5a5a5a5a5a5a5a5a5a5"
#endif

static const char *TAG = "main";

#define BTN_PIN GPIO_NUM_0


void app_main() {
    vTaskDelay(pdMS_TO_TICKS(1000)); // Delay for a second
    ESP_LOGI(TAG, "app_main()..");
    int res = 0;

    NET_Init();

    // Check that we have network settings stored..
    if(CFG_Init() == CFG_ERR_UNINTIALIZED) {
network_setup:
        // First time setup!
        NET_Start(NET_MODE_SETUP);
        
        // Start up the Fallback webserver
        FBK_Init();

        // Wait for setup to be complete..
        while (!FBK_SetupComplete()) {
            vTaskDelay(pdMS_TO_TICKS(1000)); // Delay for a second
            ESP_LOGI(TAG, "Waiting for Initial Setup...");
        }
        // Clean up the Fallback webserver
        FBK_End();
    }
    if((res = NET_Start(NET_MODE_APP)) != NET_OK) {
        // Failed to join the network, enter setup
        goto network_setup;
    }
    
    OTA_Init();

    // Configure the BOOT button GPIO
    esp_rom_gpio_pad_select_gpio(BTN_PIN);
    gpio_set_direction(BTN_PIN, GPIO_MODE_INPUT);
    gpio_set_pull_mode(BTN_PIN, GPIO_PULLUP_ONLY);

    ESP_LOGI(TAG, "Waiting for buttons!!");
    TickType_t xLastPress = 0;
    while (1) {
        if(gpio_get_level(BTN_PIN) == 0) {
            // Button pressed! store the time
            if(xLastPress == 0) {
                xLastPress = xTaskGetTickCount();
            }
        } else if(xLastPress > 0) {
            // Button released!
            uint32_t duration = (xTaskGetTickCount() - xLastPress) * portTICK_PERIOD_MS;
            ESP_LOGI("BTN", "Button Pressed for %ld ms", duration);
            if(duration > 50) {
                // Debounce..
                if(duration < 10000) {
                    // Short Press (> 50ms for debounce)
                    //  Trigger OTA checking/applying updates
                    OTA_TriggerNow();
                } else {
                    // Long Press
                    //  Clear network configuration and reboot
                    CFG_Erase(CFG_NAMESPACE_NETWORK);

                    ESP_LOGI(TAG, "Cleared network configuration, rebooting...");
                    // Busy wait for 1 second
                    ets_delay_us(1000000);
                    esp_restart();
                } 
            }
            xLastPress = 0;
        }
        vTaskDelay(10 / portTICK_PERIOD_MS); // Short delay to avoid busy waiting
    }
}