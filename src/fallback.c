#include <string.h>
#include "esp_http_server.h"
#include "esp_log.h"
#include "net.h"
#include "cfg.h"

#include "fallback.h"

static const char *TAG = "FBK";

static httpd_handle_t s_server = NULL;
static bool s_setup_complete = false;

esp_err_t fbk_getHandler(httpd_req_t *req) {
    // Serve the fallback page
    const char* resp_str = "<!DOCTYPE html><html><body><form action=\"/submit\" method=\"POST\">SSID:<br><input type=\"text\" name=\"ssid\"><br>Password:<br><input type=\"text\" name=\"password\"><br><br><input type=\"submit\" value=\"Submit\"></form></body></html>";
    httpd_resp_send(req, resp_str, HTTPD_RESP_USE_STRLEN);

    return ESP_OK;
}

char* fbk_findKeyValue(const char* data, const char* key, char* value, int value_len) {
    char* pos = strstr(data, key);
    if (pos) {
        pos += strlen(key) + 1; // Move past "key=" part
        char* end = strchr(pos, '&');
        int len = end ? (end - pos) : strlen(pos);
        len = len < value_len ? len : value_len - 1;
        strncpy(value, pos, len);
        value[len] = '\0';
        return value;
    }
    return NULL;
}

esp_err_t fbk_postHandler(httpd_req_t *req) {
    char content[200];
    int received = httpd_req_recv(req, content, req->content_len);
    if (received <= 0) { // Error or connection closed
        if (received == HTTPD_SOCK_ERR_TIMEOUT) {
            httpd_resp_send_408(req);
        }
        return ESP_FAIL;
    }

    // Ensure null-termination for string operations
    content[received] = '\0';

    char ssid[NET_MAX_LEN_SSID];
    char password[NET_MAX_LEN_PASS];

    if (fbk_findKeyValue(content, "ssid", ssid, sizeof(ssid)) && fbk_findKeyValue(content, "password", password, sizeof(password))) {
        // Save SSID and Password to NVS/CFG
        if(CFG_SaveNetworkSettings(ssid, password) == NET_OK) {
            httpd_resp_send(req, "Configuration saved", HTTPD_RESP_USE_STRLEN);
            s_setup_complete = true;
        } else {
            httpd_resp_send(req, "Failed to save configuration", HTTPD_RESP_USE_STRLEN);
        }        
    } else {
        httpd_resp_send(req, "Invalid data", HTTPD_RESP_USE_STRLEN);
    }

    return ESP_OK;
}

bool FBK_SetupComplete() {
    return s_setup_complete;
}

int FBK_Init(void) {
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();

    // Start the httpd server
    ESP_LOGI(TAG, "Starting server on port: %d", config.server_port);
    if (httpd_start(&s_server, &config) == ESP_OK) {
        // Set URI handlers
        httpd_uri_t get_uri = {
            .uri      = "/",
            .method   = HTTP_GET,
            .handler  = fbk_getHandler,
            .user_ctx = NULL
        };
        httpd_register_uri_handler(s_server, &get_uri);

        httpd_uri_t post_uri = {
            .uri      = "/submit",
            .method   = HTTP_POST,
            .handler  = fbk_postHandler,
            .user_ctx = NULL
        };
        httpd_register_uri_handler(s_server, &post_uri);
    }
    s_setup_complete = false;

    return FBK_OK;
}

void FBK_End(void) {
    if(s_server != NULL) {
        httpd_stop(s_server);
        s_server = NULL;
    }
}