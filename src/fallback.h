/*
 * FALLBACK_H
 */

#ifndef _FALLBACK_H_
#define _FALLBACK_H_

#define FBK_OK                  (0)
#define FBK_ERR                 (-1)

bool FBK_SetupComplete(void);

int  FBK_Init(void);
void FBK_End(void);

#endif//_FALLBACK_H_