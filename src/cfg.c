/* 
 * CFG.C
 *
 * Non-volatile 'configuration' storage.
 */
#include "esp_log.h"
#include "esp_system.h"
#include "nvs.h"
#include "nvs_flash.h"

#include "cfg.h"

static const char *TAG = "CFG";

int CFG_SaveNetworkSettings(char* ssid, char* password) {
    nvs_handle_t nvs_handle;
    esp_err_t err = nvs_open(CFG_NAMESPACE_NETWORK, NVS_READWRITE, &nvs_handle);

    if (err != ESP_OK) {
        return CFG_ERR_FAILED_TO_SAVE;
    }

    nvs_set_str(nvs_handle, CFG_KEY_NETWORK_SSID, ssid);
    nvs_set_str(nvs_handle, CFG_KEY_NETWORK_PASS, password);
    nvs_commit(nvs_handle);
    nvs_close(nvs_handle);

    ESP_LOGI(TAG, "Saved Network Settings!");
    
    return CFG_OK;
}

int CFG_GetNetworkSSID(char* ssid, size_t len) {
    nvs_handle_t nvs_handle;
    esp_err_t err;
    if((err = nvs_open(CFG_NAMESPACE_NETWORK, NVS_READONLY, &nvs_handle)) != ESP_OK) {
        return CFG_ERR_FAILED_TO_READ;
    }
    if((err = nvs_get_str(nvs_handle, CFG_KEY_NETWORK_SSID, ssid, &len)) != ESP_OK) {
        goto close;
    }
close:
    nvs_close(nvs_handle);
    if (err != ESP_OK) {
        return CFG_ERR_FAILED_TO_READ;
    }
    return CFG_OK;
}

int CFG_GetNetworkPassword(char* password, size_t len) {
    nvs_handle_t nvs_handle;
    esp_err_t err;
    if((err = nvs_open(CFG_NAMESPACE_NETWORK, NVS_READONLY, &nvs_handle)) != ESP_OK) {
        goto close;
    }
    if((err = nvs_get_str(nvs_handle, CFG_KEY_NETWORK_PASS, password, &len)) != ESP_OK) {
        goto close;
    }
close:
    nvs_close(nvs_handle);
    if (err != ESP_OK) {
        return CFG_ERR_FAILED_TO_READ;
    }
    return CFG_OK;
}

int CFG_Erase(const char * namespace) {
    esp_err_t err;
    nvs_handle_t nvs_handle;
    if ((err = nvs_open(namespace, NVS_READWRITE, &nvs_handle)) != ESP_OK) {
        // Could not open it, must be erased already
        return CFG_OK;
    }
    if((err = nvs_erase_all(nvs_handle)) != ESP_OK) {
        goto close;
    }
    err = nvs_commit(nvs_handle);
close:
    nvs_close(nvs_handle);
    if(err != ESP_OK) {
        return CFG_ERR;
    }
    return CFG_OK;
}

// CFG_Init()
//     Initialize non-volatile storage (NVS)
// 
// Returns: 
// - CFG_OK (0)
//      Configuration was loaded successfully
// - CFG_ERR_UNINTIALIZED (-2)
//      Configuration data is empty / uninitialized
int CFG_Init(void) {
    int res = CFG_OK;

    // Initialize NVS.
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        // 1.OTA app partition table has a smaller NVS partition size than the non-OTA
        // partition table. This size mismatch may cause NVS initialization to fail.
        // 2.NVS partition contains data in new format and cannot be recognized by this version of code.
        // If this happens, we erase NVS partition and initialize NVS again.
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();

        // NVS was cleared, Device is uninitialized
        res = CFG_ERR_UNINTIALIZED;
    }
    ESP_ERROR_CHECK(err);
    
    // Check that we can load the Network namespace
    nvs_handle_t nvs_handle;
    if((err = nvs_open(CFG_NAMESPACE_NETWORK, NVS_READONLY, &nvs_handle)) != ESP_OK) {
        res = CFG_ERR_UNINTIALIZED;
    } else {
        nvs_close(nvs_handle);
    }

    return res;
}