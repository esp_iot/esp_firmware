/*
 * NET_H
 */

#ifndef _NET_H_
#define _NET_H_

#include "esp_wifi.h"

#define NET_OK                      (0)
#define NET_ERR                     (-1)
#define NET_ERR_LOADING_CFG         (-2) // Error while loading configuration data (SSID, PASS, etc)
#define NET_ERR_FAILED_TO_CONNECT   (-3) // Failed to connect to the configured SSID/PASS

#define NET_MODE_SETUP      (0) // Setup/SOFT_AP mode, for initial device setup
#define NET_MODE_APP        (1) // Application mode, for normal operation

#define NET_MAXIMUM_RETRY   (6) // Number of connect attempts before fallingback hotspot is started

#define NET_MAX_LEN_SSID    (32)
#define NET_MAX_LEN_PASS    (32)

bool NET_Connected(void);
const char * NET_GetIPString(void);
void NET_SetPowerSaving(wifi_ps_type_t psType);
int  NET_Init();
int  NET_Start(int netMode);
void NET_End();

#endif//_NET_H_