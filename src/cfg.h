/*
 * CFG_H
 */

#ifndef _CFG_H_
#define _CFG_H_

#define CFG_OK                  (0)
#define CFG_ERR                 (-1)    // Unspecified error
#define CFG_ERR_UNINTIALIZED    (-2)    // Device is uninitialized
#define CFG_ERR_FAILED_TO_SAVE  (-3)
#define CFG_ERR_FAILED_TO_READ  (-4)

#define CFG_NAMESPACE_NETWORK   "net"

#define CFG_KEY_NETWORK_SSID    "ssid"
#define CFG_KEY_NETWORK_PASS    "pass"

int CFG_SaveNetworkSettings(char* ssid, char* password);
int CFG_GetNetworkSSID(char* ssid, size_t len);
int CFG_GetNetworkPassword(char* password, size_t len);
int CFG_Erase(const char * namespace);
int CFG_Init(void);

#endif//_CFG_H_