/* 
 * NET.C
 *
 * Network functions
 */

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_wifi.h"
#include "esp_log.h"
#include "esp_netif.h"
#include "esp_event.h"
#include "esp_mac.h"
#include "esp_http_server.h"
#include "cfg.h"

#include "net.h"

// The SSID of the fallback/initial setup AP
//  This will be followed by the last 4 characters of the device MAC
//  ie. ESPIOT_1A2B
// You can override this with -DNET_FALLBACK_SSID_PREFIX during build
#ifndef NET_FALLBACK_SSID_PREFIX
#define NET_FALLBACK_SSID_PREFIX    "ESPIOT"
#endif//NET_FALLBACK_SSID_PREFIX

// The password for the fallback/initial setup AP
//  Leave this undefined to use OPEN authentication
// You can override/set this with -DNET_FALLBACK_PASSWORD during build
#ifndef NET_FALLBACK_PASSWORD
//#define NET_FALLBACK_PASSWORD       "p@Ssw0rd"
#endif//NET_FALLBACK_PASSWORD

static const char *TAG = "NET";

static uint8_t s_mac[6];
static char s_ip_str[16];
static bool s_connected = false;

/* FreeRTOS event group to signal when we are connected*/
static EventGroupHandle_t s_wifi_event_group;
/* The event group allows multiple bits for each event, but we only care about two events:
 * - we are connected to the AP with an IP
 * - we failed to connect after the maximum amount of retries */
#define WIFI_CONNECTED_BIT BIT0
#define WIFI_FAIL_BIT      BIT1

static int s_retry_num = 0;

static void net_eventHandler(void* arg, esp_event_base_t event_base,
                                int32_t event_id, void* event_data)
{
    if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START) {
        esp_wifi_connect();
    } else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED) {
        if (s_retry_num < NET_MAXIMUM_RETRY) {
            esp_wifi_connect();
            s_retry_num++;
            ESP_LOGI(TAG, "retry to connect to the AP");
        } else {
            xEventGroupSetBits(s_wifi_event_group, WIFI_FAIL_BIT);
        }
        ESP_LOGI(TAG,"connect to the AP fail");
    } else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP) {
        ip_event_got_ip_t* event = (ip_event_got_ip_t*) event_data;
        ESP_LOGI(TAG, "got ip:" IPSTR, IP2STR(&event->ip_info.ip));
        s_retry_num = 0;
        xEventGroupSetBits(s_wifi_event_group, WIFI_CONNECTED_BIT);
    }
}

// NET_Connected()
//     Indicates if the device is connected to the configured network
//
// Returns:
//  - true/false:  If connected/not connected
bool NET_Connected(void) {
    return s_connected;
}

const char * NET_GetIPString(void) {
    return (const char*)s_ip_str;
}

void NET_SetPowerSaving(wifi_ps_type_t psType) {
    ESP_ERROR_CHECK(esp_wifi_set_ps(psType));
}


// NET_Start(netMode)
//     Initialize the network interface
// 
// Parameters:
// - netMode (int)
//      NET_MODE_SETUP (0): Device hosts a SoftAP for initial setup
//      NET_MODE_APP   (1): Device joins the network specified in NVS/CFG
//
// Returns: 
// - NET_OK (0)
//      Network interface was loaded successfully
// - NET_ERR (-1)
//      Unspecified network error
// - NET_ERR_FAILED_TO_CONNECT (-3)
//      Failed to connect to configured network as client
//
// Note:
//  This will disconnect/stop any existing wifi connections before starting
int NET_Start(int netMode) {
    esp_wifi_disconnect();
    esp_wifi_stop();
    s_connected = false;

    if(netMode == NET_MODE_SETUP) {
        // Initial Setup
        //  Start a SoftAP with ~device-unique SSID
        esp_netif_create_default_wifi_ap();

        wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
        ESP_ERROR_CHECK(esp_wifi_init(&cfg));


        char fallbackSSID[NET_MAX_LEN_SSID];
        snprintf(fallbackSSID, sizeof(fallbackSSID), "%s_%02X%02X", NET_FALLBACK_SSID_PREFIX, s_mac[4], s_mac[5]);

        // Wifi configuration
        //  Authentication mode depends on if NET_FALLBACK_PASSWORD is defined
        wifi_config_t fallbackConfig = {
            .ap = {
                .ssid_len = strlen(fallbackSSID),
                .channel = 1,
                .max_connection = 4,
            #ifdef NET_FALLBACK_PASSWORD
                .authmode = WIFI_AUTH_WPA_WPA2_PSK,
                .password = NET_FALLBACK_PASSWORD,
            #else //NET_FALLBACK_PASSWORD
                .authmode = WIFI_AUTH_OPEN,
            #endif//NET_FALLBACK_PASSWORD
            },
        };
        memcpy(fallbackConfig.ap.ssid, fallbackSSID, sizeof(fallbackConfig.ap.ssid));

        ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_AP));
        ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_AP, &fallbackConfig));
        ESP_ERROR_CHECK(esp_wifi_start());

        s_connected = true;

        // Save the current ip address
        esp_netif_ip_info_t ip_info;
        esp_netif_t *netif = esp_netif_get_handle_from_ifkey("WIFI_AP_DEF");
        if (netif == NULL) {
            ESP_LOGE(TAG, "Failed to get interface handle for SoftAP");
            return NET_ERR;
        }
        if (esp_netif_get_ip_info(netif, &ip_info) == ESP_OK) {
            esp_ip4addr_ntoa(&ip_info.ip, s_ip_str, sizeof(s_ip_str));
        } else {
            s_ip_str[0] = '\0';
            ESP_LOGE(TAG, "Failed to get IP address for SoftAP");
        }

        ESP_LOGI(TAG, "Started Setup AP: %s @ %s", fallbackSSID, NET_GetIPString());
    } else if (netMode == NET_MODE_APP) {
        // Main Application
        //  Try to connect to the network stored in CFG/NVS
        esp_netif_create_default_wifi_sta();

        wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
        ESP_ERROR_CHECK(esp_wifi_init(&cfg));

        s_wifi_event_group = xEventGroupCreate();
        esp_event_handler_instance_t instance_any_id;
        esp_event_handler_instance_t instance_got_ip;
        ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT,
                                                            ESP_EVENT_ANY_ID,
                                                            &net_eventHandler,
                                                            NULL,
                                                            &instance_any_id));
        ESP_ERROR_CHECK(esp_event_handler_instance_register(IP_EVENT,
                                                            IP_EVENT_STA_GOT_IP,
                                                            &net_eventHandler,
                                                            NULL,
                                                            &instance_got_ip));
        
        wifi_config_t wifi_config = {};
        if((CFG_GetNetworkSSID((char*)wifi_config.sta.ssid, sizeof(wifi_config.sta.ssid)) != CFG_OK) ||
            (CFG_GetNetworkPassword((char*)wifi_config.sta.password, sizeof(wifi_config.sta.password)) != CFG_OK)) {
            ESP_LOGE(TAG, "Failed to load AP Credentials!");
            return NET_ERR_LOADING_CFG;
        }
        wifi_config.sta.threshold.authmode = WIFI_AUTH_WPA2_PSK;

        ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
        ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config));
        ESP_ERROR_CHECK(esp_wifi_start());

        // Waiting until either the connection is established (WIFI_CONNECTED_BIT) or connection failed for the maximum
        //  number of re-tries (WIFI_FAIL_BIT). The bits are set by event_handler() (see above)
        EventBits_t bits = xEventGroupWaitBits(s_wifi_event_group,
                WIFI_CONNECTED_BIT | WIFI_FAIL_BIT,
                pdFALSE,
                pdFALSE,
                portMAX_DELAY);

        // xEventGroupWaitBits() returns the bits before the call returned, hence we can test which event actually
        // happened.
        if (bits & WIFI_CONNECTED_BIT) {
            // Success! Continue..
            s_connected = true;
        } else if (bits & WIFI_FAIL_BIT) {
            ESP_LOGI(TAG, "Failed to connect to SSID:%s, password:%s",
                    wifi_config.sta.ssid, wifi_config.sta.password);
            s_connected = false;
            return NET_ERR_FAILED_TO_CONNECT;
        } else {
            ESP_LOGE(TAG, "UNEXPECTED EVENT");
            s_connected = false;
            return NET_ERR;
        }
        
        // Save the current ip address
        esp_netif_ip_info_t ip_info;
        esp_netif_t *netif = esp_netif_get_handle_from_ifkey("WIFI_STA_DEF");
        if (netif == NULL) {
            ESP_LOGE(TAG, "Failed to get interface handle for STA interface");
            return NET_ERR;
        }
        // Get the IP information for the station interface
        if (esp_netif_get_ip_info(netif, &ip_info) == ESP_OK) {
            // Convert the IP address to a string
            esp_ip4addr_ntoa(&ip_info.ip, s_ip_str, sizeof(s_ip_str));
        } else {
            ESP_LOGE(TAG, "Unable to get IP information for the STA interface");
            s_ip_str[0] = '\0';
        }

        ESP_LOGI(TAG, "Connected to %s @ %s", wifi_config.sta.ssid, NET_GetIPString());
    }
    return NET_OK;
}

// NET_Init()
//     Initialize the network interface, call NET_Start() to setup the network
//
// Returns: 
// - NET_OK (0)
//      Network interface was loaded successfully
// 
// Notes:
//  Call this only ONCE
int NET_Init(int netMode) {
    ESP_ERROR_CHECK(esp_netif_init());
    ESP_ERROR_CHECK(esp_event_loop_create_default());

    // Read the device MAC address
    ESP_ERROR_CHECK(esp_read_mac(s_mac, ESP_MAC_WIFI_SOFTAP));

    return NET_OK;
}

// NET_Init(netMode)
//     Deinitializes the network
void NET_End(void) {

    esp_wifi_disconnect();
    s_connected = false;

    esp_wifi_stop();
    esp_wifi_deinit();
}