/* OTA.h

    This file is based off the "simple_ota_example.c" provided by espressif. Here's their
    original license agreement:

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_ota_ops.h"
#include "esp_http_client.h"
#include "esp_https_ota.h"
#include "string.h"
#include "esp_crt_bundle.h"
#include <sys/socket.h>
#include "net.h"

#include "ota.h"

/* Macro for the FULL COMMIT HASH of the current build */
#ifndef VERSION_BUILD_COMMIT
#define VERSION_BUILD_COMMIT "a5a5a5a5a5a5a5a5a5a5a5a5a5a5a5a5a5a5a5a5"
#endif

static const char *TAG = "OTA";

extern const uint8_t server_cert_pem_start[] asm("_binary_ca_cert_pem_start");
extern const uint8_t server_cert_pem_end[] asm("_binary_ca_cert_pem_end");

static TaskHandle_t _otaTaskHandle;
static otaState_t _otaState = OTA_IDLE;
static SemaphoreHandle_t _triggerSemephore;

esp_err_t _http_event_handler(esp_http_client_event_t *evt)
{
    switch (evt->event_id) {
    case HTTP_EVENT_ERROR:
        ESP_LOGD(TAG, "HTTP_EVENT_ERROR");
        break;
    case HTTP_EVENT_ON_CONNECTED:
        ESP_LOGD(TAG, "HTTP_EVENT_ON_CONNECTED");
        break;
    case HTTP_EVENT_HEADER_SENT:
        ESP_LOGD(TAG, "HTTP_EVENT_HEADER_SENT");
        break;
    case HTTP_EVENT_ON_HEADER:
        ESP_LOGD(TAG, "HTTP_EVENT_ON_HEADER, key=%s, value=%s", evt->header_key, evt->header_value);
        break;
    case HTTP_EVENT_ON_DATA:
        ESP_LOGD(TAG, "HTTP_EVENT_ON_DATA, len=%d", evt->data_len);
        if (!esp_http_client_is_chunked_response(evt->client)) {
            // If user_data buffer is provided, copy the response into the buffer
            if (evt->user_data) {
                strncat((char*)evt->user_data, (const char*)evt->data, evt->data_len);
            }
        }
        break;
    case HTTP_EVENT_ON_FINISH:
        ESP_LOGD(TAG, "HTTP_EVENT_ON_FINISH");
        break;
    case HTTP_EVENT_DISCONNECTED:
        ESP_LOGD(TAG, "HTTP_EVENT_DISCONNECTED");
        break;
    case HTTP_EVENT_REDIRECT:
        ESP_LOGD(TAG, "HTTP_EVENT_REDIRECT");
        break;
    }
    return ESP_OK;
}

// Function to initialize and perform an HTTPS GET request
static esp_err_t https_get_request(const char *url, int *status_code, char *rsp_buf, int *rsp_len) {
    esp_http_client_config_t config = {
        .url = url,
        .cert_pem = (char *)server_cert_pem_start,
        .event_handler = _http_event_handler,
        .user_data = rsp_buf,
        .keep_alive_enable = true,
    };

#ifdef CONFIG_OTA_SKIP_COMMON_NAME_CHECK
    config.skip_cert_common_name_check = true;
#endif


    esp_http_client_handle_t client = esp_http_client_init(&config);
    esp_err_t err = esp_http_client_perform(client);

    if (err == ESP_OK) {
        //ESP_LOGI("HTTPS", "HTTPS GET Status = %d, content_length = %d",
        //         esp_http_client_get_status_code(client),
        //             (int)esp_http_client_get_content_length(client));
        if(status_code) {
           *status_code = esp_http_client_get_status_code(client);
        }
        if(rsp_len) {
            *rsp_len = (int)esp_http_client_get_content_length(client);
        }
    } else {
        ESP_LOGE("HTTPS", "HTTPS GET request failed: %s", esp_err_to_name(err));
    }
    esp_http_client_cleanup(client);
    return err;
}

int  ota_CheckForUpdate()
{
    esp_err_t err = ESP_OK;

    char    url[OTA_URL_SIZE];

    int     status_code = 0;
    char    rsp_buf[OTA_CHECK_BUF_SIZE] = {0};
    int     rsp_len = 0;

    ESP_LOGI(TAG, "Checking for Update ('%s')", CONFIG_OTA_PROJECT_KEY);

    // Build the 'CHECK' request URL
    //  `fw.example.com:3000/check/project_key`
    snprintf(url, sizeof(url),
        "%s/check/%s", CONFIG_OTA_UPGRADE_URL, CONFIG_OTA_PROJECT_KEY);
    // Request the current <hash> for the OTA_PROJECT_KEY
    if ((err = https_get_request(url, &status_code, rsp_buf, &rsp_len)) != ESP_OK) {
        // Failed to check 
        ESP_LOGE(TAG, "ERROR: Failed update check, request error: %s)", esp_err_to_name(err));
        return OTA_ERROR;
    }

    // Check the result, expecting 200
    if(status_code != 200) {
        ESP_LOGE(TAG, "ERROR: Failed update check, invalid status_code: %d)", status_code);
        return OTA_ERROR;
    }

    // Check the response length, expecting > 0
    if(rsp_len <= 0) {
        ESP_LOGE(TAG, "ERROR: Failed update check, invalid response length: %d)", rsp_len);
        return OTA_ERROR;
    }

    // Compare with VERSION_BUILD_COMMIT
    rsp_buf[rsp_len] = 0; // Ensure null termination
    if(strcmp(rsp_buf, VERSION_BUILD_COMMIT) == 0) {
        // Versions match, Up To Date!
        ESP_LOGI(TAG, "Up To Date (%.7s..)", rsp_buf);
        return OTA_OK;
    } else {
        // Versions mismatch, Update Available!
        ESP_LOGI(TAG, "Update Available (%.7s.. -> %.7s..)", VERSION_BUILD_COMMIT, rsp_buf);
        return OTA_UPDATE_AVAILABLE;
    }
}

void ota_UpdateNow(void) {

    ESP_LOGI(TAG, "Downloading Update ('%s')", CONFIG_OTA_PROJECT_KEY);
    char url[OTA_URL_SIZE];

    // Build the 'FETCH' request URL
    //  `fw.example.com:3000/fetch/project_key`
    snprintf(url, OTA_URL_SIZE,
        "%s/fetch/%s", CONFIG_OTA_UPGRADE_URL, CONFIG_OTA_PROJECT_KEY);

    esp_http_client_config_t config = {
        .url = url,
        .cert_pem = (char *)server_cert_pem_start,
        .event_handler = _http_event_handler,
        .keep_alive_enable = true,
    };
    #ifdef CONFIG_OTA_SKIP_COMMON_NAME_CHECK
        config.skip_cert_common_name_check = true;
    #endif

    esp_https_ota_config_t ota_config = {
        .http_config = &config,
    };
        
    NET_SetPowerSaving(WIFI_PS_NONE);

    ESP_LOGI(TAG, "Downloading update from %s", config.url);
    esp_err_t ret = esp_https_ota(&ota_config);
    if (ret == ESP_OK) {
        ESP_LOGI(TAG, "Update Complete! Rebooting...");
        esp_restart();
    } else {
        ESP_LOGE(TAG, "ERROR: Upgrade failed!");
    }
}

void ota_Task(void *pvParameter)
{
    ESP_LOGI(TAG, "OTA Update Task");

    TickType_t xLastWakeTime = xTaskGetTickCount();
    
    while(1) {
        // Wait for the next trigger or timeout
        xSemaphoreTake(_triggerSemephore, OTA_UPDATE_INTERVAL_MS);
        
        switch(_otaState) {
        case(OTA_IDLE): {
            // Idle - Wait for update to be available
            //  --> (OTA_CHECKING_FOR_UPDATE) --> OTA_UPDATE_AVAILABLE
            _otaState = OTA_CHECKING_FOR_UPDATE;
            if(ota_CheckForUpdate() == OTA_UPDATE_AVAILABLE) {
                _otaState = OTA_UPDATE_AVAILABLE;
            } else {
                _otaState = OTA_IDLE;
            }
            break;
        }
        case(OTA_UPDATE_AVAILABLE): {
            // Update Available - Apply update
            //  --> (OTA_UPDATING) --> esp_restart()
            _otaState = OTA_UPDATING;

            // Increase the priority!
            vTaskPrioritySet(_otaTaskHandle, 7);

            ota_UpdateNow();
            break;
        }
        default:
            ESP_LOGE(TAG, "Invalid State: %d! Going to OTA_IDLE", _otaState);
            _otaState = OTA_IDLE;
            break;
        }

        // Update xLastWakeTime for daily scheduling, if you use vTaskDelayUntil for precise timing
        xLastWakeTime += OTA_UPDATE_INTERVAL_MS;
    }
}

// OTA_TriggerNow()
//     Sets the OTA task to check for, or download/install an update
//
// Returns:
//  Nothing
void OTA_TriggerNow(void) {
    // Trigger the task to run immediately (If there is something to do)
    if(_otaState == OTA_IDLE || _otaState == OTA_UPDATE_AVAILABLE) {
        xSemaphoreGive(_triggerSemephore);
    }
}

// OTA_UpdateAvailable()
//     Checks the *internal OTA state* to see if an update is available
//
// Returns:
//  - true:  An update is available and ready to be downloaded
//  - false: An update is not available, or not ready to be downloaded
bool OTA_UpdateAvailable(void) {
    return (_otaState == OTA_UPDATE_AVAILABLE);
}

// OTA_Init()
//     Initialize the OTA module/tasks
// 
// Returns: 
// - OTA_OK (0)
//      OTA updates were setup / started successfully
int OTA_Init(void) {
    _triggerSemephore = xSemaphoreCreateBinary();
    if (_triggerSemephore == NULL) {
        // TODO: Handle error
    }

    // Start the OTA task (With low priority)
    //  This will check for/apply updates according to OTA_UPDATE_INTERVAL_MS
    //   or when/if OTA_TriggerNow() is called.
    //  The task will first check for updates and apply the update on the next
    //   intervale/trigger.
    xTaskCreate(&ota_Task, "OTA Task", 8192, NULL, 1, &_otaTaskHandle);

    return OTA_OK;
}