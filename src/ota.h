/*
 * OTA_H
 */

#ifndef _OTA_H_
#define _OTA_H_


#define OTA_OK                  (0)
#define OTA_ERROR               (-1)

#define OTA_URL_SIZE            (256)
#define OTA_CHECK_BUF_SIZE      (1024)
#define OTA_UPDATE_INTERVAL_MS  (24 * 60 * 60 * 1000) // Run once a day

typedef enum otaState_t {
    OTA_IDLE,
    OTA_CHECKING_FOR_UPDATE,
    OTA_UPDATE_AVAILABLE,
    OTA_UPDATING
} otaState_t;


void OTA_TriggerNow(void);
bool OTA_UpdateAvailable(void);
int  OTA_Init(void);

#endif//_OTA_H_