# Simple script to automatically download the root certificate from the UPGRADE_URL 
#  defined in the current sdkconfig (set using menuconfig)

# Import platformio environment
Import("env")

import subprocess
import tempfile
import os
from pathlib import Path
try:
    from cryptography import x509
    from cryptography.hazmat.backends import default_backend
    from cryptography.hazmat.primitives import serialization
except ImportError:
    env.Execute("$PYTHONEXE -m pip install cryptography")
    
    raise ImportError("Installed missing packages (cryptography) please retry the build")

def get_ota_upgrade_url_from_config(config_filename) -> str:
    with open(config_filename, "r") as config:
        for line in config.readlines():
            line = line.strip()
            if(line != '' and not line.startswith("#")):
                key, value = line.split("=", 1)
                if(key == "CONFIG_OTA_UPGRADE_URL"):
                    value = value.strip('"').strip("'")
                    return value
            
    raise Exception(f"Failed to find 'CONFIG_OTA_UPGRADE_URL=...' in {config_filename}. Make sure you set this variable using menuconfig first!")

def get_root_certificate(url: str) -> str:
    # Extract hostname
    hostname = url.split('://')[1]

    # Create a temporary file manually without automatic deletion
    temp_file = tempfile.NamedTemporaryFile(delete=False)
    temp_file_name = temp_file.name
    temp_file.close()  # Close the file so OpenSSL can write to it

    try:
        # Use OpenSSL to fetch the certificate chain
        subprocess.check_call(['openssl', 's_client', '-showcerts', '-servername', f'{hostname}', '-connect', f'{hostname}:443'],
                              stdout=open(temp_file_name, 'w'), stderr=subprocess.DEVNULL)

        # Read and parse the certificate chain
        with open(temp_file_name, 'r') as cert_file:
            cert_data = cert_file.read()

        # Isolate certificates
        certs = []
        begin = "-----BEGIN CERTIFICATE-----"
        end = "-----END CERTIFICATE-----"
        start_idx = cert_data.find(begin)
        while start_idx != -1:
            end_idx = cert_data.find(end, start_idx) + len(end)
            cert_pem = cert_data[start_idx:end_idx]
            if cert_pem:
                certs.append(cert_pem)
            start_idx = cert_data.find(begin, end_idx)

        # Parse each certificate to find a self-signed certificate (root)
        root_cert = None
        for cert_pem in certs:
            cert_obj = x509.load_pem_x509_certificate(cert_pem.encode(), default_backend())
            if cert_obj.issuer == cert_obj.subject:  # Simple check for a self-signed certificate
                root_cert = cert_obj
                break

        # Failed to find self-signed certificate. Just return the longest usable certificate
        if not root_cert:
            for cert_pem in certs:
                cert_obj = x509.load_pem_x509_certificate(cert_pem.encode(), default_backend())
                if root_cert == None or cert_obj.not_valid_after_utc > root_cert.not_valid_after_utc:
                    root_cert = cert_obj

        if root_cert:
            print(f"Root Certificate Issuer: {root_cert.issuer}")
            print(f"Certificate Valid until: {root_cert.not_valid_after_utc}")
            return root_cert.public_bytes(serialization.Encoding.PEM).decode()
        else:
            print("Root certificate could not be found.")
            return "Root certificate could not be found."
    finally:
        # Ensure the temporary file is removed after use
        os.remove(temp_file_name)

cert_path = Path("server_certs/ca_cert.pem")
if(not cert_path.exists()):
    # Get the OTA_UPGRADE_URL from configuration file (for the current environment)
    config_filename = f"sdkconfig.{env['PIOENV']}"
    upgrade_url = get_ota_upgrade_url_from_config(config_filename)
    print(f"Getting Certificate for: {upgrade_url}")

    # Assuming the URL is in the correct format, if not, additional parsing may be required.
    root_cert_pem = get_root_certificate(upgrade_url)

    # Save the certificate
    print(f"Saving Certificate to: {cert_path}")
    cert_path.parent.mkdir(parents=True, exist_ok=True)
    cert_path.write_text(root_cert_pem)
else:
    cert = x509.load_pem_x509_certificate(cert_path.read_bytes(), default_backend())
    print(f"'{cert_path}' already exists. Delete this file to fetch a new one")
    print(f"Root Certificate Issuer: {cert.issuer}")
    print(f"Certificate Valid until: {cert.not_valid_after_utc}")