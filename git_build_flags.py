# Simple script to include the current GIT COMMIT HASH as the "VERSION_BUILD_COMMIT" macro in the project

from pickle import NONE
import subprocess

COMMIT_FULL = (
    subprocess.check_output(["git", "rev-parse", "HEAD"])
    .strip()
    .decode("utf-8")
)

print(f"-DVERSION_BUILD_COMMIT='\"{COMMIT_FULL}\"' ")