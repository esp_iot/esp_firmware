Import("env")

def get_ota_project_key_from_config(config_filename) -> str:
    with open(config_filename, "r") as config:
        for line in config.readlines():
            line = line.strip()
            if(line != '' and not line.startswith("#")):
                key, value = line.split("=", 1)
                if(key == "CONFIG_OTA_PROJECT_KEY"):
                    value = value.strip('"').strip("'")
                    return value
            
    raise Exception(f"Failed to find 'CONFIG_OTA_PROJECT_KEY=...' in {config_filename}. Make sure you set this variable using menuconfig first!")


config_filename = f"sdkconfig.{env['PIOENV']}"
project_key = get_ota_project_key_from_config(config_filename)

board = env.BoardConfig()
mcu = board.get("build.mcu", "esp32")

env.AddPostAction(
    "$BUILD_DIR/${PROGNAME}.elf",
    env.VerboseAction(" ".join([
        '"$PYTHONEXE" "$OBJCOPY"',
        "--chip", mcu, "elf2image",
        "--flash_mode", "${__get_board_flash_mode(__env__)}",
        "--flash_freq", "${__get_board_f_flash(__env__)}",
        "--flash_size", board.get("upload.flash_size", "4MB"),
        "-o", f"$PROJECT_DIR/{project_key}.bin", "$BUILD_DIR/${PROGNAME}.elf"
    ]), f"Building $PROJECT_DIR/{project_key}.bin")
)