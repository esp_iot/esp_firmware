# ESP_IOT - ESP Firmware

This project implements a basic ESP32 device which supports OTA upgrades using the [ESP_IOT firmware server](https://gitlab.com/esp_iot/firmware_server).

## Features

- **Check for Update**: Supports basic update checking by comparing the build-time commit hash against the hash uploaded to the firmware server.
- **Download Updates**: If an update is available, the device can download and apply the update automatically (or manually via push button).
- **NVS Configuration**: Uses Non-Volatile Storage (NVS) for wifi configuration. Allows individual device settings to be different from the firmware server.
- **Setup/Fallback Hotspot**: Extremely basic setup webpage and hotspot for initial device setup or fallback when WiFi connection is unavailable.
- **HTTPS/Certificate Checks**: firmware_server certificate is automatically downloaded and stored on the ESP32. This is used to verify the firmware_server for all requests (`check` and `fetch`)

## Getting Started

### Prerequisites

### Installation

1. Clone the repository and import into Platformio

2. Run MenuConfig to configure ESP_IOT settings

    1. Go into `ESP_IOT Configuration` settings.
    2. Enter `firmware_server` address (Example: `https://fw.example.com`). This should be the base address for your [ESP_IOT firmware server](https://gitlab.com/esp_iot/firmware_server)
    3. Enter `project_key` for this device (Default: `esp_firmware`). This is the key used to fetch this device's firmware.

### Gitlab CI/CD Setup

A basic .gitlab-ci.yml file is provided for both building firmware files and commiting them to your firmware_server.

For building, you should have these variables defined:

- `PROJECT_KEY`: The key used to commit/fetch updates for this project on the firmware_server. Must match your sdkconfig set with MenuConfig. Default is "esp_firmware"
- `COMMIT_HASH` (Optional): The hash used to commit/fetch the latest file for this project. Must match the output of git_build_flags.py. Default is given by $CI_COMMIT_SHA

---

### Compiling and Deploying

#### Build with Platformio (Local)

1. Build the project using Platformio

2. (*Optional*) Upload directly to connected device

3. (*Optional*) Commit to [firmware_server](https://gitlab.com/esp_iot/firmware_server) using 

    1. After building, `export_built_image.py` will save the compiled binary as a `./${PROJECT_KEY}.bin` for use with firmware_server (Example: `./esp_firmware.bin`)
    2. Commit the file to [firmware_server](https://gitlab.com/esp_iot/firmware_server/-/blob/main/README.md?ref_type=heads#usage) using `commit_file.sh`

#### Gitlab CI/CD Building

For deploying to a firmware_server server, you must have these variables defined:

- `SERVER_ADDR` (**Required**): The address of the firmware_server. This must be defined to use the deploy- stage
- `SERVER_PORT` (Optional): The port of the firmware_server. Default is 443

If your server uses Authenication (`REQUIRE_AUTH==true`), you must define these variables. The default is to deploy without authentication (leave these blank if this is your case).

- `OAUTH_URI` (Optional): The .well-known/openid-configuration url for your OAUTH2.0 authentication server. Leave blank (default) to commit files without authentication
- `OAUTH_CLIENTID` (Required if `OAUTH_URI` is provided): OAUTH2.0 ClientID used for this project
- `OAUTH_USERNAME` (Required if `OAUTH_URI` is provided): Username to use when fetching a token
- `OAUTH_PASSWORD` (Required if `OAUTH_URI` is provided): Password to use when fetching a token

## Contributing
We welcome contributions! Please open an issue or submit a pull request for any improvements or feature additions.